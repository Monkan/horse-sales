//
//  HorsesTableViewController.m
//  HorseSales
//
//  Created by ITHS on 2016-04-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "HorsesTableViewController.h"
#import "HorseDetailViewController.h"
#import "Horse.h"
#import "AddViewController.h"

@interface HorsesTableViewController ()

@property  () NSMutableArray* jumpingHorses;
@property ()UILabel* horseNameLabel;
@property (strong) Horse* currentHorse;
@property () NSArray* searchResults;
@property UISearchController *searchController;

@end

@implementation HorsesTableViewController


- (void)viewDidAppear:(BOOL)animated {
    
    NSData *data =[[NSUserDefaults standardUserDefaults]objectForKey:@"sampleHorse"];
    if(data == nil){
        self.jumpingHorses = [[NSMutableArray alloc]init];
    } else {
        self.jumpingHorses = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData *data =[[NSUserDefaults standardUserDefaults]objectForKey:@"sampleHorse"];
    if(data == nil){
        self.jumpingHorses = [[NSMutableArray alloc]init];
    } else {
        self.jumpingHorses = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }

    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    NSLog(@"%lu", (unsigned long)[self.jumpingHorses count]);
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchText = searchController.searchBar.text;
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"horseName contains[c] %@", searchText];
    NSPredicate *father = [NSPredicate predicateWithFormat:@"horseFather contains[c] %@", searchText];
    NSPredicate *category = [NSPredicate predicateWithFormat:@"horseCategory contains[c] %@", searchText];
    NSPredicate *compoundPred = [NSCompoundPredicate orPredicateWithSubpredicates:@[resultPredicate, father, category]];
    
    self.searchResults= self.jumpingHorses;
    self.searchResults = [self.jumpingHorses filteredArrayUsingPredicate:compoundPred];
    [self.tableView reloadData];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.searchController.isActive && self.searchController.searchBar.text > 0){
        return self.searchResults.count;
    }
    else{
        return self.jumpingHorses.count;
    }
    
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self tableView:[self tableView] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"cell_top.png"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"cell_bottom.png"];
    } else {
        background = [UIImage imageNamed:@"cell_middle.png"];
    }
    
    return background;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    if (self.searchController.isActive && self.searchController.searchBar.text > 0){
        
        self.currentHorse = self.searchResults[indexPath.row];
        UIImageView *horseImageView = (UIImageView *)[cell viewWithTag:100];
        horseImageView.image = [UIImage imageNamed:@"Ellen"];
        
        self.horseNameLabel = (UILabel *)[cell viewWithTag:101];
        self.horseNameLabel.text = self.currentHorse.horseName;
        
        
        UILabel *fatherLabel = (UILabel *)[cell viewWithTag:102];
        fatherLabel.text = self.currentHorse.horseFather;
        
        UILabel *heightLabel = (UILabel *)[cell viewWithTag:103];
        heightLabel.text = self.currentHorse.horseHeight;
        
        UILabel *priceLabel = (UILabel *)[cell viewWithTag:104];
        priceLabel.text = self.currentHorse.horsePrice;
        
    } else {
        self.currentHorse = self.jumpingHorses[indexPath.row];
        
        UIImageView *horseImageView = (UIImageView *)[cell viewWithTag:100];
        horseImageView.image = [UIImage imageNamed:@"Ellen"];
        
        self.horseNameLabel = (UILabel *)[cell viewWithTag:101];
        self.horseNameLabel.text = self.currentHorse.horseName;
        
        
        UILabel *fatherLabel = (UILabel *)[cell viewWithTag:102];
        fatherLabel.text = self.currentHorse.horseFather;
        
        UILabel *heightLabel = (UILabel *)[cell viewWithTag:103];
        heightLabel.text = self.currentHorse.horseHeight;
        
        UILabel *priceLabel = (UILabel *)[cell viewWithTag:104];
        priceLabel.text = self.currentHorse.horsePrice;
        
    }
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        NSMutableArray *horsesToDeleteWith = [[NSMutableArray alloc]initWithArray:self.jumpingHorses];
        [horsesToDeleteWith removeObjectAtIndex:indexPath.row];
        self.jumpingHorses = [horsesToDeleteWith copy];
        
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:self.jumpingHorses];
        [userDefault setObject:myEncodedObject forKey:@"sampleHorse"];
        [userDefault synchronize];
        
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
        
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UITableViewCell *)sender {
    if ([segue.identifier isEqualToString:@"HorseDetail"]) {
        
        HorseDetailViewController *destination = [segue destinationViewController];
        
        if (self.searchController.isActive && self.searchController.searchBar.text > 0){
            NSInteger selected = [[self.tableView indexPathForSelectedRow]row];
            destination.detailHorse = [self.searchResults objectAtIndex:selected];
        }
        else
        {
            HorseDetailViewController *destination = [segue destinationViewController];
            destination.title = self.horseNameLabel.text;
            NSInteger selected = [[self.tableView indexPathForSelectedRow]row];
            destination.detailHorse = [self.jumpingHorses objectAtIndex:selected];
        }
        
        
    }
    else if ([segue.identifier isEqualToString:@"AddHorse"]) {
        AddViewController *destination = [segue destinationViewController];
        destination.tableViewHorses = self.jumpingHorses;
    }
    
}


@end