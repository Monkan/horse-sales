//
//  HorsesTableViewController.h
//  HorseSales
//
//  Created by ITHS on 2016-04-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Horse.h"

@interface HorsesTableViewController : UITableViewController <UISearchResultsUpdating>

@property () Horse *horse;



@end
