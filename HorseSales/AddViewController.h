//
//  AddViewController.h
//  HorseSales
//
//  Created by ITHS on 2016-04-19.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Horse.h"

@interface AddViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property()Horse *addedHorse;
@property () NSMutableArray *tableViewHorses;

-(NSString*)imagePath;

@end
