//
//  HorseDetailViewController.h
//  HorseSales
//
//  Created by ITHS on 2016-04-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HorsesTableViewController.h"
#import <MessageUI/MessageUI.h>

@interface HorseDetailViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *sendEmail;
- (IBAction)sendEmail:(id)sender;


@property () Horse* detailHorse;

@end
