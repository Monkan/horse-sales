//
//  StartViewController.m
//  HorseSales
//
//  Created by ITHS on 2016-04-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "StartViewController.h"
#import "BackgroundColorLayer.h"

@interface StartViewController ()

@property (weak, nonatomic) IBOutlet UIButton *jumpHorseButton;

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.startImageView.layer.cornerRadius = self.startImageView.frame.size.width / 2;
    self.startImageView.clipsToBounds = YES;
    
    self.startImageLabel.layer.cornerRadius = self.startImageLabel.frame.size.width / 2;
    self.startImageLabel.clipsToBounds = YES;
    self.startImageView.layer.borderWidth = 3.0f;
    self.startImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    CAGradientLayer *bgLayer = [BackgroundColorLayer blueGradient];
    bgLayer.frame = self.view.bounds;
    [self.view.layer insertSublayer:bgLayer atIndex:0];
    

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
