//
//  HorseDetailViewController.m
//  HorseSales
//
//  Created by ITHS on 2016-04-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "HorseDetailViewController.h"
#import "BackgroundColorLayer.h"
#import "AddViewController.h"


@interface HorseDetailViewController () 

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatherLabel;
@property (weak, nonatomic) IBOutlet UILabel *gFatherLabel;
@property (weak, nonatomic) IBOutlet UILabel *bornYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UITextField *licenseNrText;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
@property (weak, nonatomic) IBOutlet UIButton *contactBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@end

@implementation HorseDetailViewController

- (void)viewDidAppear:(BOOL)animated {
    
    UIImage *image = [UIImage imageWithContentsOfFile:self.detailHorse.horseImageName];
    NSLog(@"%@", self.detailHorse.horseImageName);
    self.imageView.image = image;
    
    [self.view reloadInputViews];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    CAGradientLayer *bgLayer = [BackgroundColorLayer blueGradient];
    bgLayer.frame = self.view.bounds;
    [self.view.layer insertSublayer:bgLayer atIndex:0];
    
    self.priceLabel.text = self.detailHorse.horseName;
    self.fatherLabel.text = self.detailHorse.horseFather;
    self.gFatherLabel.text = self.detailHorse.horseGrandF;
    self.bornYearLabel.text = self.detailHorse.horseYearOfBirth;
    self.sexLabel.text = self.detailHorse.horseGender;
    self.infoTextView.text = self.detailHorse.horseDescription;
    
    //UIImage *image = [UIImage imageWithContentsOfFile:self.detailHorse.horseImageName];
   // NSLog(@"%@", self.detailHorse.horseImageName);
    //self.imageView.image = image;
    
}

-(UIImage *)loadImage:(NSString *)imageName{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path =paths[0];
        UIImage *image = [UIImage imageWithContentsOfFile:path];
        [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    
    return image;
}
- (void)sendEmail:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"Hästannons på horseConnect"];
        [mail setMessageBody:@"Test mail" isHTML:NO];
        [mail setToRecipients:@[@"monika.svensson@tele2.se"]];
        
        [self presentViewController:mail animated:YES completion:NULL];
        
    } else {
        
        NSLog(@"This device cannot send email");
        
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
 

/*#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
