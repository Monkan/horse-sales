//
//  Horse.h
//  HorseSales
//
//  Created by ITHS on 2016-04-19.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Horse : NSObject
{
    NSString *name;
    NSString *horseDescription;
    NSString *horseFather;
    NSString *horseGrandF;
    NSString *horseYearOfBirth;
    NSString *horsePrice;
    NSString *horseContact;
    NSString *horseCategory;
    NSString *horseGender;
    NSString *horseHeight;
    NSString *horseImageName;
    //UIImage *horseimage;
}

@property(nonatomic, retain) NSString *horseName;
@property(nonatomic, retain) NSString *horseDescription;
@property(nonatomic, retain) NSString *horseFather;
@property(nonatomic, retain) NSString *horseGrandF;
@property(nonatomic, retain) NSString *horseYearOfBirth;
@property(nonatomic, retain) NSString *horsePrice;
@property(nonatomic, retain) NSString *horseContact;
@property(nonatomic, retain) NSString *horseCategory;
@property(nonatomic, retain) NSString *horseGender;
@property(nonatomic, retain) NSString *horseHeight;
@property(nonatomic, retain) NSString *horseImageName;
//@property(nonatomic, strong) UIImage *horseImage;

@end
