//
//  Horse.m
//  HorseSales
//
//  Created by ITHS on 2016-04-19.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "Horse.h"

@implementation Horse
@synthesize horseName;
@synthesize horseDescription;
@synthesize horseFather;
@synthesize horseGrandF;
@synthesize horsePrice;
@synthesize horseContact;
@synthesize horseCategory;
@synthesize horseGender;
@synthesize horseHeight;
@synthesize horseYearOfBirth;
@synthesize horseImageName;
//@synthesize horseImage;


-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.horseName forKey:@"name"];
    [encoder encodeObject:self.horseDescription forKey:@"description"];
    [encoder encodeObject:self.horseFather forKey:@"father"];
    [encoder encodeObject:self.horseGrandF forKey:@"grandfather"];
    [encoder encodeObject:self.horsePrice forKey:@"price"];
    [encoder encodeObject:self.horseYearOfBirth forKey:@"year"];
    [encoder encodeObject:self.horseContact forKey:@"contact"];
    [encoder encodeObject:self.horseCategory forKey:@"category"];
    [encoder encodeObject:self.horseGender forKey:@"gender"];
    [encoder encodeObject:self.horseHeight forKey:@"height"];
    [encoder encodeObject:self.horseImageName forKey:@"image"];
    //[encoder encodeObject:self.horseImage forKey:@"ihorseImage"];



}

-(id)initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if( self != nil )
    {
        self.horseName = [decoder decodeObjectForKey:@"name"];
        self.horseDescription = [decoder decodeObjectForKey:@"description"];
        self.horseFather = [decoder decodeObjectForKey:@"father"];
        self.horseGrandF = [decoder decodeObjectForKey:@"grandfather"];
        self.horsePrice = [decoder decodeObjectForKey:@"price"];
        self.horseYearOfBirth = [decoder decodeObjectForKey:@"year"];
        self.horseContact = [decoder decodeObjectForKey:@"contact"];
        self.horseCategory = [decoder decodeObjectForKey:@"category"];
        self.horseGender = [decoder decodeObjectForKey:@"gender"];
        self.horseHeight = [decoder decodeObjectForKey:@"height"];
        self.horseImageName = [decoder decodeObjectForKey:@"image"];
        //self.horseImage = [decoder decodeObjectForKey:@"horseImage"];


    }
    return self;

}

@end
