//
//  StartViewController.h
//  HorseSales
//
//  Created by ITHS on 2016-04-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *startImageView;
@property (weak, nonatomic) IBOutlet UILabel *startImageLabel;

@end
