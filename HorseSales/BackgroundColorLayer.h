//
//  BackgroundColorLayer.h
//  HorseSales
//
//  Created by ITHS on 2016-04-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface BackgroundColorLayer : NSObject

+(CAGradientLayer*) greyGradient;
+(CAGradientLayer*) blueGradient;

@end
