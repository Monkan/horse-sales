//
//  AddViewController.m
//  HorseSales
//
//  Created by ITHS on 2016-04-19.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "BackgroundColorLayer.h"
#import "AddViewController.h"
#import "Horse.h"
#import "HorseDetailViewController.h"
#import "HorsesTableViewController.h"

@interface AddViewController ()

@property (strong, nonatomic) IBOutlet UITextField *addName;
@property (strong, nonatomic) IBOutlet UITextField *addFather;
@property (strong, nonatomic) IBOutlet UITextField *addGrandFather;
@property (strong, nonatomic) IBOutlet UITextField *addGender;
@property (strong, nonatomic) IBOutlet UITextField *addCategory;
@property (strong, nonatomic) IBOutlet UITextField *addPrice;
@property (strong, nonatomic) IBOutlet UITextField *addAge;
@property (strong, nonatomic) IBOutlet UITextView *addInfo;
@property (strong, nonatomic) IBOutlet UITextField *addEmail;
@property (strong, nonatomic) IBOutlet UITextField *addHeight;
@property (weak, nonatomic) IBOutlet UIImageView *myImage;

@property (nonatomic)NSString *imageDataPath;



@property () NSData *imageData;
@property () UIImage *horseImage;
@property ()NSString *path;


@end

@implementation AddViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData *data =[[NSUserDefaults standardUserDefaults]objectForKey:@"sampleHorse"];
    self.tableViewHorses = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if(self.tableViewHorses == nil){
        self.tableViewHorses= [[NSMutableArray alloc]init];
    }
    
    CAGradientLayer *bgLayer = [BackgroundColorLayer blueGradient];
    bgLayer.frame = self.view.bounds;
    [self.view.layer insertSublayer:bgLayer atIndex:0];
}

- (IBAction)addImage:(id)sender {

    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    self.horseImage = info[UIImagePickerControllerEditedImage];
    
    self.myImage.image = self.horseImage;
    
    self.imageData = UIImagePNGRepresentation(self.horseImage);
    self.path = [self imagePath];
    
    
    BOOL success =[self.imageData writeToFile:[self imagePath] atomically:YES];
    if (success) {
        NSLog(@"Saved image to user document directory.");
    }else {
        NSLog(@"Failed to save image.");
    }

}
-(NSString*)imagePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path =paths[0];
    NSString *imageName = self.addName.text;
    return [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
 
}
- (IBAction)addHorse:(id)sender {
 
    Horse *addedHorse = [[Horse alloc]init];
                        addedHorse.horseName =self.addName.text;
                        addedHorse.horseDescription=self.addInfo.text;
                        addedHorse.horseFather=self.addFather.text;
                        addedHorse.horseGrandF=self.addGrandFather.text;
                        addedHorse.horseYearOfBirth=self.addAge.text;
                        addedHorse.horsePrice=self.addPrice.text;
                        addedHorse.horseContact=self.addEmail.text;
                        addedHorse.horseCategory=self.addCategory.text;
                        addedHorse.horseGender=self.addGender.text;
                        addedHorse.horseHeight=self.addHeight.text;
                        addedHorse.horseImageName = self.path;
    
    
                        NSLog(@"Image path %@",addedHorse.horseImageName);

    
    [self.tableViewHorses addObject:addedHorse];
    
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:self.tableViewHorses];
    [userDefault setObject:myEncodedObject forKey:@"sampleHorse"];
    [userDefault synchronize];
  
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
/*#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}*/


@end


